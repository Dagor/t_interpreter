#ifndef _FRAME_HPP_INCLUDED
#define _FRAME_HPP_INCLUDED

#include "Constant.hpp"
#include <stack>
#include <map>
#include <string>

namespace Interpreter
{
  /**
   * 
   * Frame class.
   * 
   * The stack frame for the interpreter.
   */
  class Frame 
  {
    std::stack<Constant> m_stack; //!stack
    std::map<std::string, Constant> m_variables; //!variables
    unsigned int m_programCounter; //!previous program counter state
    public:
    Frame(const std::stack<Constant> &stack, 
      const std::map<std::string, Constant> &vars, unsigned int pc);
    /**
     * Stack getter function.
     * 
     * @return stack with values
     */
    std::stack<Constant> getStack() const { return m_stack; }

    /**
     * Getter function for variables.
     * 
     * @return map containing variables
     */
    std::map<std::string, Constant> getVars() const { return m_variables; }

    /**
     * Program counter getter function.
     * 
     * @return unsigned integer with PC value.
     */
    unsigned int getProgramCounter() const { return m_programCounter; }
  };
}

#endif