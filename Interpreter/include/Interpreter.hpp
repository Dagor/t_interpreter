#ifndef _INTERPRETER_HPP_INCLUDED
#define _INTERPRETER_HPP_INCLUDED

#include "Instruction.hpp"
#include "Constant.hpp"
#include "InstructionContainer.hpp"
#include "Frame.hpp"
#include <vector>
#include <stack>
#include <map>

namespace Interpreter
{
  /**
   * 
   * Interpreter class.
   * 
   * Class contains instructions, labels and other content
   * required to act as a virtual machine.
   * Can interpret the intermediate language.
   * 
   */
  class Interpreter
  {
    std::map<std::string, Constant> m_global_vars; //!global variables
    std::map<std::string, Constant> m_local_vars;  //!local variables

    std::vector<InterLang::Instruction> m_instructions; //!instructions
    std::map<std::string, unsigned int> m_labels;  //!labels for instructions
    unsigned int m_nextInstr;  //!index of next instruction to execute
    std::stack<Constant> m_stack;  //!stack containing values

    std::stack<Frame> m_frames;  //!contains all saved stack frames

    bool m_isFinished;  //!flag indicating that program is finished

    std::stack<Constant> m_passed_args; //!arguments passed to another function

    //instructions operating on variables
    void LOAD_CONST(const InterLang::Argument &arg);
    void LOAD_VAR(const InterLang::Argument &arg);
    void STORE_VAR(const InterLang::Argument &arg);
    void CREATE_VAR(const InterLang::Argument &arg);

    //operations operating on two variables
    void ADD();
    void SUB();
    void MULTIPLY();
    void DIVIDE();
    void COMPARE_EQ();
    void COMPARE_NEQ();
    void COMPARE_LESS();
    void COMPARE_LESS_EQ();
    void COMPARE_MORE();
    void COMPARE_MORE_EQ();

    //operations operating on one variable
    void NEGATE();
    void POP();


    //jumps
    void JUMP_IF_FALSE(const InterLang::Argument &arg);
    void JUMP(const InterLang::Argument &arg);

    //subroutine calls
    void CALL(const InterLang::Argument &arg);
    void RETURN();
    void RETURN_WITH_VAL();
    void PASS_ARG();
    void LOAD_PASSED();

    //other functions
    void CLEAR_STACK();

    //console writing and reading
    void PRINTLN();
    void CONSOLE_GET_INT();
    public:
    Interpreter(const InterLang::InstructionContainer &container);
    void Interpret();
  };
}

#endif