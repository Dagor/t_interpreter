#ifndef _CONSTANT_HPP_INCLUDED
#define _CONSTANT_HPP_INCLUDED

#include <variant>
#include <string>

namespace Interpreter
{
    typedef enum
    {
      INTEGER,
      STRING,
      BOOLEAN
    }ConstantType;

    /**
     * 
     * Constant class.
     * 
     * Class contains the type an value of the constant or variable.
     */
    class Constant
    {
      ConstantType m_type;  //!type
      std::variant<int, std::string, bool> m_val;  //!value
      public:
      Constant();
      Constant(int val);
      Constant(const std::string &val);
      Constant(bool val);
      ConstantType getType() const { return m_type; }
      int getInt() const;
      std::string getString() const;
      bool getBool() const;

    };
}

#endif