#include "Interpreter.hpp"
#include <iostream>

namespace Interpreter
{
  /**
   * Constructor of interpreter class.
   * 
   * @param container containing instructions and labels
   */
  Interpreter::Interpreter(const InterLang::InstructionContainer &container) :
    m_instructions(container.instructions), m_labels(container.labels),
    m_nextInstr(0), m_stack(), m_isFinished(false)
    {
    }

  /**
   * 
   * Functions starts interpreting the instructions.
   * 
   * Function dispatches instructions to proper functions.
   * 
   */
  void Interpreter::Interpret()
  {
      while(!m_isFinished)
      {
          InterLang::Instruction instr(m_instructions[m_nextInstr]);
          if(instr.getOperation() == InterLang::LOAD_CONST)
          {
            LOAD_CONST(instr.getArg());
            continue;
          }
          if(instr.getOperation() == InterLang::ADD)
          {
            ADD();
            continue;
          }
          if(instr.getOperation() == InterLang::SUB)
          {
            SUB();
            continue;
          }
          if(instr.getOperation() == InterLang::NEGATE)
          {
            NEGATE();
            continue;
          }
          if(instr.getOperation() == InterLang::POP)
          {
            POP();
            continue;
          }
          if(instr.getOperation() == InterLang::MUL)
          {
            MULTIPLY();
            continue;
          }
          if(instr.getOperation() == InterLang::DIV)
          {
            DIVIDE();
            continue;
          }
          if(instr.getOperation() == InterLang::CREATE_VAR)
          {
            CREATE_VAR(instr.getArg());
            continue;
          }
          if(instr.getOperation() == InterLang::LOAD_VAR)
          {
            LOAD_VAR(instr.getArg());
            continue;
          }
          if(instr.getOperation() == InterLang::STORE_VAR)
          {
            STORE_VAR(instr.getArg());
            continue;
          }
          if(instr.getOperation() == InterLang::CLEAR_STACK)
          {
            CLEAR_STACK();
            continue;
          }
          if(instr.getOperation() == InterLang::COMPARE_EQ)
          {
            COMPARE_EQ();
            continue;
          }
          if(instr.getOperation() == InterLang::COMPARE_NEQ)
          {
            COMPARE_NEQ();
            continue;
          }
          if(instr.getOperation() == InterLang::COMPARE_LESS)
          {
            COMPARE_LESS();
            continue;
          }
          if(instr.getOperation() == InterLang::COMPARE_LESS_EQ)
          {
            COMPARE_LESS_EQ();
            continue;
          }
          if(instr.getOperation() == InterLang::COMPARE_MORE)
          {
            COMPARE_MORE();
            continue;
          }
          if(instr.getOperation() == InterLang::COMPARE_MORE_EQ)
          {
            COMPARE_MORE_EQ();
            continue;
          }
          if(instr.getOperation() == InterLang::JUMP_IF_FALSE)
          {
            JUMP_IF_FALSE(instr.getArg());
            continue;
          }
          if(instr.getOperation() == InterLang::JUMP)
          {
            JUMP(instr.getArg());
            continue;
          }
          if(instr.getOperation() == InterLang::CALL)
          {
            CALL(instr.getArg());
            continue;
          }
          if(instr.getOperation() == InterLang::RETURN)
          {
            RETURN();
            continue;
          }
          if(instr.getOperation() == InterLang::RETURN_WITH_VAL)
          {
            RETURN_WITH_VAL();
            continue;
          }
          if(instr.getOperation() == InterLang::PASS_ARG)
          {
            PASS_ARG();
            continue;
          }
          if(instr.getOperation() == InterLang::LOAD_PASSED)
          {
            LOAD_PASSED();
            continue;
          }
          if(instr.getOperation() == InterLang::PRINTLN)
          {
            PRINTLN();
            continue;
          }
          if(instr.getOperation() == InterLang::CONSOLE_GET_INT)
          {
            CONSOLE_GET_INT();
            continue;
          }
      }
  }

  /**
   * 
   * Load constant instruction.
   * 
   * Loads constant onto stack.
   * 
   * @param arg instruction argument containing constant
   * 
   */
  void Interpreter::LOAD_CONST(const InterLang::Argument &arg)
  {
    if(arg.getType() == InterLang::INT_CONST)
    {
      int val = arg.getInt();
      m_stack.push(Constant(val));
    }
    else if(arg.getType() == InterLang::STRING_CONST)
    {
      std::string val = arg.getString();
      m_stack.push(Constant(val));
    }
    else
      throw std::runtime_error("Unknown type.");
    m_nextInstr++;
  }


  /**
   * 
   * Load variable instruction.
   * 
   * Function loads the variable onto stack
   * 
   * @param arg instruction argument containing variable name
   * 
   */
  void Interpreter::LOAD_VAR(const InterLang::Argument &arg)
  {
    if (arg.getType() == InterLang::VAR_NAME)
    {
      std::string varName(arg.getString());
      if(m_local_vars.count(varName))
      {
        m_stack.push(m_local_vars.at(varName));
      }
      else if (m_global_vars.count(varName))
      {
        m_stack.push(m_global_vars.at(varName));
      }
      else
        throw std::runtime_error("Variable does not exist.");
    }
    else
      throw std::runtime_error("Argument is not a variable name");

    m_nextInstr++;
  }


  /**
   * 
   * Store variable instruction.
   * 
   * Stores the value from the stack to the variable, then pops
   * the variable off the stack.
   * 
   * @param arg instruction argument containing variable name
   * 
   */
  void Interpreter::STORE_VAR(const InterLang::Argument &arg)
  {
    if (arg.getType() == InterLang::VAR_NAME)
    {
      std::string varName(arg.getString());
      if(m_local_vars.count(varName))
      {
        m_local_vars[varName] = m_stack.top();
        m_stack.pop();
      }
      else if (m_global_vars.count(varName))
      {
        m_global_vars[varName] = m_stack.top();
        m_stack.pop();
      }
      else
        throw std::runtime_error("Variable does not exist.");
    }
    else
      throw std::runtime_error("Argument is not a variable name");

    m_nextInstr++;
  }

  /**
   * 
   * Create variable instruction.
   * 
   * Function allocates variable.
   * 
   * @param arg instruction argument containing variable name
   * 
   */
  void Interpreter::CREATE_VAR(const InterLang::Argument &arg)
  {
    if(arg.getType() == InterLang::VAR_NAME)
    {
      std::string varName(arg.getString());
      if(!m_local_vars.count(varName))
      {
        m_local_vars[varName] = Constant();
      }
      else
        throw std::runtime_error("Variable already exists.");
    }
    else
      throw std::runtime_error("Argument is not a variable name.");

    m_nextInstr++;
  }

  /**
   * 
   * Add instruction.
   * 
   * Instruction pops two values off the stack and adds them.
   * Can add both string and integer values.
   * 
   */
  void Interpreter::ADD()
  {
    //pop the values off the stack
    Constant val2 = m_stack.top();
    m_stack.pop();
    Constant val1 = m_stack.top();
    m_stack.pop();

    //concatenate strings
    if(val1.getType() == STRING)
    {
      //get both values as strings
      std::string str1 = val1.getString();
      std::string str2 = val2.getString();
      std::string out = str1 + str2;
      //push the result back onto stack
      m_stack.push(Constant(out));
    }
    else if(val1.getType() == INTEGER)
    {
        //integer to integer addition
        if(val2.getType() == INTEGER)
        {
          int int1 = val1.getInt();
          int int2 = val2.getInt();
          int out = int1 + int2;
          m_stack.push(Constant(out));
        }
        //string concatenation
        else if(val2.getType() == STRING)
        {
          std::string str1 = val1.getString();
          std::string str2 = val2.getString();
          std::string out = str1 + str2;
          m_stack.push(Constant(out));
        }
        else
        {
          throw std::runtime_error("Unknown type.");
        }
    }
    else
    {
        throw std::runtime_error("Unknown type.");
    }
    m_nextInstr++;
  }
  
  /**
   * 
   * Subtract instruction.
   * 
   * Pops two values off the stack and subtracts them. The result is pushed
   * back onto the stack. Only works on integer values.
   * 
   */
  void Interpreter::SUB()
  {
    //pop the values off the stack
    Constant val2 = m_stack.top();
    m_stack.pop();
    Constant val1 = m_stack.top();
    m_stack.pop();

    //currently the interpreter only supports integers
    if (val1.getType() != INTEGER && val2.getType() != INTEGER)
    {
      throw std::runtime_error("Cannot subtract non integer types.");
    }

    //add the values
    int operand1 = val1.getInt();
    int operand2 = val2.getInt();
    int result = operand1 - operand2;

    //push the result onto stack
    m_stack.push(Constant(result));

    //increment instruction counter
    m_nextInstr++;
  }

  /**
   * 
   * Multiply instruction.
   * 
   * Function pops two values from the stack then multiplies them.
   * The result is pushed back onto the stack. Works only on integers.
   * 
   */
  void Interpreter::MULTIPLY()
  {
    //pop values off the stack
    Constant val2 = m_stack.top();
    m_stack.pop();
    Constant val1 = m_stack.top();
    m_stack.pop();

    if(val1.getType() == INTEGER && val2.getType() == INTEGER)
    {
      //multiply the values
      int operand1 = val1.getInt();
      int operand2 = val2.getInt();
      int out = operand1 * operand2;
      //push the result on the stack
      m_stack.push(Constant(out));
    }
    else
      throw std::runtime_error("Both of the values have to be integer.");

    m_nextInstr++;
  }

  /**
   * 
   * Divide instruction.
   * 
   * Function pops two values from the stack then divides them.
   * The result is pushed back onto the stack. Works only on integers.
   * 
   */
  void Interpreter::DIVIDE()
  {
    Constant val2 = m_stack.top();
    m_stack.pop();
    Constant val1 = m_stack.top();
    m_stack.pop();

    if(val1.getType() == INTEGER && val2.getType() == INTEGER)
    {
      int operand1 = val1.getInt();
      int operand2 = val2.getInt();
      int out = operand1 / operand2;
      m_stack.push(Constant(out));
    }
    else
      throw std::runtime_error("Both of the values have to be integer.");

    m_nextInstr++;
  }

  /**
   * 
   * Compare equal instruction.
   * 
   * Pops two values off the stack and compares them.
   * If they are equal pushes true onto the stack else pushes false.
   * Can compare only integers.
   * 
   */
  void Interpreter::COMPARE_EQ()
  {
    //get values off the stack
    Constant val2 = m_stack.top();
    m_stack.pop();
    Constant val1 = m_stack.top();
    m_stack.pop();

    if(val1.getType() == INTEGER && val2.getType() == INTEGER)
    {
      bool out = (val1.getInt() == val2.getInt());
      m_stack.push(Constant(out));
    }
    else
      throw std::runtime_error("Invalid operands.");

    m_nextInstr++;
  }

  /**
   * 
   * Compare not equal instruction.
   * 
   * Pops two values off the stack and compares them.
   * If they are not equal pushes true onto the stack else pushes false.
   * Can compare only integers.
   * 
   */
  void Interpreter::COMPARE_NEQ()
  {
    //get values off the stack
    Constant val2 = m_stack.top();
    m_stack.pop();
    Constant val1 = m_stack.top();
    m_stack.pop();

    if(val1.getType() == INTEGER && val2.getType() == INTEGER)
    {
      bool out = (val1.getInt() != val2.getInt());
      m_stack.push(Constant(out));
    }
    else
      throw std::runtime_error("Invalid operands.");

    m_nextInstr++;
  }

  /**
   * 
   * Compare not equal instruction.
   * 
   * Pops two values off the stack and compares them.
   * If left value is less than right one pushes true onto stack else
   * pushes false.
   * Can compare only integers.
   * 
   */
  void Interpreter::COMPARE_LESS()
  {
    //get values off the stack
    Constant val2 = m_stack.top();
    m_stack.pop();
    Constant val1 = m_stack.top();
    m_stack.pop();

    if(val1.getType() == INTEGER && val2.getType() == INTEGER)
    {
      bool out = (val1.getInt() < val2.getInt());
      m_stack.push(Constant(out));
    }
    else
      throw std::runtime_error("Invalid operands.");

    m_nextInstr++;
  }

  /**
   * 
   * Compare less or equal instruction.
   * 
   * Compares the two top stack values.
   * If the left value is less or equal to right value pushes true onto
   * the stack. Otherwise pushes false.
   */
  void Interpreter::COMPARE_LESS_EQ()
  {
    //get values off the stack
    Constant val2 = m_stack.top();
    m_stack.pop();
    Constant val1 = m_stack.top();
    m_stack.pop();

    if(val1.getType() == INTEGER && val2.getType() == INTEGER)
    {
      bool out = (val1.getInt() <= val2.getInt());
      m_stack.push(Constant(out));
    }
    else
      throw std::runtime_error("Invalid operands.");

    m_nextInstr++;
  }

  /**
   * 
   * Compare more instruction.
   * 
   * Compares the two top stack values.
   * If the left value higher than right value pushes true onto
   * the stack. Otherwise pushes false.
   */
  void Interpreter::COMPARE_MORE()
  {
    //get values off the stack
    Constant val2 = m_stack.top();
    m_stack.pop();
    Constant val1 = m_stack.top();
    m_stack.pop();

    if(val1.getType() == INTEGER && val2.getType() == INTEGER)
    {
      bool out = (val1.getInt() > val2.getInt());
      m_stack.push(Constant(out));
    }
    else
      throw std::runtime_error("Invalid operands.");

    m_nextInstr++;
  }

  /**
   * 
   * Compare more or equal instruction.
   * 
   * Compares the two top stack values.
   * If the left value is more or equal to right value pushes true onto
   * the stack. Otherwise pushes false.
   */
  void Interpreter::COMPARE_MORE_EQ()
  {
    //get values off the stack
    Constant val2 = m_stack.top();
    m_stack.pop();
    Constant val1 = m_stack.top();
    m_stack.pop();

    if(val1.getType() == INTEGER && val2.getType() == INTEGER)
    {
      bool out = (val1.getInt() >= val2.getInt());
      m_stack.push(Constant(out));
    }
    else
      throw std::runtime_error("Invalid operands.");

    m_nextInstr++;
  }

  /**
   * 
   * Negate instruction.
   * 
   * Function gets the top value from the stack and negates it.
   * The top value gets pushed back onto the stack.
   * Works only on integers.
   */
  void Interpreter::NEGATE()
  {
    //get value from the stack
    Constant val = m_stack.top();
    m_stack.pop();

    if (val.getType() == INTEGER)
    {
      int value = val.getInt();
      m_stack.push(Constant(-value));
    }
    else
    {
      throw std::runtime_error("Can only negate integer values");
    }

    m_nextInstr++;
  }

  /**
   * 
   * Pop instruction.
   * 
   * Pops the value off the stack.
   * 
   */
  void Interpreter::POP()
  {
    m_stack.pop();
  }

  /**
   * Jump if false instruction.
   * 
   * Functions jumps to label if the top value on the stack is false.
   * 
   * @param arg instruction argument containing the label to which to jump
   * 
   */
  void Interpreter::JUMP_IF_FALSE(const InterLang::Argument &arg)
  {
    if(arg.getType() == InterLang::LABEL)
    {
      Constant condition = m_stack.top();
      m_stack.pop();

      if(condition.getType() == BOOLEAN)
      {
        if(condition.getBool())
          m_nextInstr++;
        else
          m_nextInstr = m_labels.at(arg.getString());
      }
      else
        throw std::runtime_error("Jump condition must be a boolean.");
    }
    else
      throw std::runtime_error("Argument must be a label");
  }

  /**
   * Jump instruction.
   * 
   * Functions jumps to label unconditionally.
   * 
   * @param arg instruction argument containing the label to which to jump
   * 
   */
  void Interpreter::JUMP(const InterLang::Argument &arg)
  {
    if(arg.getType() == InterLang::LABEL)
    {
      m_nextInstr = m_labels.at(arg.getString());
    }
    else
      throw std::runtime_error("Argument must be a label.");
  }

  /**
   * Call instruction.
   * 
   * Functions calls the subroutine.
   * The current stack frame (instruction counter, stack with values
   * and variables) is saved to stack containing frames.
   * 
   * @param arg instruction argument containing the subroutine label.
   * 
   */
  void Interpreter::CALL(const InterLang::Argument &arg)
  {
    if(arg.getType() != InterLang::LABEL)
    {
      throw
        std::runtime_error("Argument to call must be a label.");
    }
    //save current stack frame
    Frame current_frame(m_stack, m_local_vars, m_nextInstr + 1);
    m_frames.push(current_frame);

    //jump to label
    m_nextInstr = m_labels.at(arg.getString());
    while(!m_stack.empty())
      m_stack.pop();

    //clean the local variables
    m_local_vars = std::map<std::string, Constant>();
    
  }

  /**
   * 
   * Return from subroutine instruction.
   * 
   * Function returns from subroutine.
   * If the stack containing frames is empty sets the isFinished flag.
   * If it is not empty restores the last frame.
   * 
   */
  void Interpreter::RETURN()
  {
    //indicate that the program finished
    if(m_frames.empty())
      m_isFinished = true;
    else
    {
      //restore the stack frame
      m_local_vars = m_frames.top().getVars();
      m_nextInstr = m_frames.top().getProgramCounter();
      m_stack = m_frames.top().getStack();

      m_frames.pop();
    }

    while(!m_passed_args.empty())
    {
      m_passed_args.pop();
    }
  }

  /**
   * 
   * Return from subroutine with value instruction.
   * 
   * Function returns from subroutine.
   * If the stack containing frames is empty sets the isFinished flag.
   * If it is not empty restores the last frame.
   * The top value from the stack gets pushed back onto the loaded stack.
   * 
   */

  void Interpreter::RETURN_WITH_VAL()
  {
    //read return value from stack
    Constant retVal = m_stack.top();

    //indicate that program is finished
    if(m_stack.empty())
      m_isFinished = true;
    else
    {
      //restore the previous stack frame
      m_local_vars = m_frames.top().getVars();
      m_nextInstr = m_frames.top().getProgramCounter();
      m_stack = m_frames.top().getStack();
      
      m_frames.pop();

      //push the returned value onto the stack
      m_stack.push(retVal);
    }

    while(!m_passed_args.empty())
    {
      m_passed_args.pop();
    }
  }

  /**
   * Pass argument instruction.
   * 
   * Passes arguments to other function.
   */
  void Interpreter::PASS_ARG()
  {
    //get the value from stack
    Constant val = m_stack.top();
    m_stack.pop();

    //store the value onto passed arguments stack
    m_passed_args.push(val);

    m_nextInstr++;
  }

  /**
   * 
   * Load passed instruction.
   * 
   * Loads argument passed from other function and stores it on the stack.
   */
  void Interpreter::LOAD_PASSED()
  {
    //get passed argument
    Constant val = m_passed_args.top();
    m_passed_args.pop();

    //push the value back on the stack
    m_stack.push(val);

    m_nextInstr++;
  }

  /**
   * 
   * Clear stack instruction.
   * 
   * Removes all values from the stack.
   */
  void Interpreter::CLEAR_STACK()
  {
    while(!m_stack.empty())
      m_stack.pop();

    m_nextInstr++;
  }

  /**
   * 
   * println built in function.
   * 
   * Takes the passed argument and prints it to stdout
   * with a newline.
   * 
   */
  void Interpreter::PRINTLN()
  {
    //take the argument from the m_passed_args
    Constant out(m_passed_args.top());
    m_passed_args.pop();

    //Print the value
    if(out.getType() == INTEGER)
    {
      std::cout << out.getInt() << std::endl;
    }
    else if(out.getType() == STRING)
    {
      std::cout << out.getString() << std::endl;
    }
    else if(out.getType() == BOOLEAN)
    {
      std::cout << out.getBool() << std::endl;
    }

    //clean the stack
    while(!m_passed_args.empty())
      m_passed_args.pop();

    m_nextInstr++;
  }

  /**
   * 
   * console_get_int built in function.
   * 
   * Reads integer from the console.
   * 
   */
  void Interpreter::CONSOLE_GET_INT()
  {
    int input;
    std::cin >> input;
    
    m_stack.push(Constant(input));

    while(!m_passed_args.empty())
      m_passed_args.top();
    
    m_nextInstr++;
  }
  
}