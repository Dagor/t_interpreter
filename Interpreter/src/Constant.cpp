#include "Constant.hpp"

namespace Interpreter
{
  /**
   * Constant default constructor.
   */
  Constant::Constant() : 
  m_type(INTEGER), m_val(0)
  {}

  /**
   * Constant constructor.
   * 
   * @param val integer value
   */
  Constant::Constant(int val) :
    m_type(INTEGER), m_val(val)
  {
  }

  /**
   * Constant constructor.
   * 
   * @param val string value
   */
  Constant::Constant(const std::string &val) :
    m_type(STRING), m_val(val)
  {
  }

  /**
   * Constant constructor.
   * 
   * @param val boolean value
   */
  Constant::Constant(bool val) : 
    m_type(BOOLEAN), m_val(val)
  {
  }

  /**
   * Integer getter function.
   * 
   * @return int integer value
   */
  int Constant::getInt() const
  {
    //get integer
    return std::get<int>(m_val);
  }

  /**
   * 
   * String value getter function.
   * 
   * @return string value
   */
  std::string Constant::getString() const
  {
    std::string ret;

    //get string value
    try
    {
        ret = std::get<std::string>(m_val);
    }
    //if this is not possible convert integer to string
    catch (std::exception &e)
    {
        (void)e;
        int val = std::get<int>(m_val);
        ret = std::to_string(val);
    }
    return ret;
  }

  /**
   * Get boolean value.
   * 
   * @return bool value
   */
  bool Constant::getBool() const
  {
    return std::get<bool>(m_val);
  }

}

