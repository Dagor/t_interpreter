#include "Frame.hpp"

namespace Interpreter
{
  /**
   * 
   * Frame constructor.
   * 
   * @param stack with values
   * @param vars variables
   * @param pc program counter
   */
  Frame::Frame(const std::stack<Constant> &stack, 
    const std::map<std::string, Constant> &vars, unsigned int pc) :
    m_stack(stack), m_variables(vars), m_programCounter(pc)
  {}
}