#ifndef _OPERAND_HPP_INCLUDED
#define _OPERAND_HPP_INCLUDED

#include "ASTBase.hpp"

#include <variant>
#include <string>
#include "InstructionContainer.hpp"

namespace AST
{
  /**
   * Operand node for literals.
   */
  class Operand: public ASTBase
  {
    std::variant<int, std::string> m_operand;
    public:
    Operand(int val);
    Operand(const std::string &str);
    virtual void compile(InterLang::InstructionContainer &container) override;
  };
}

#endif
