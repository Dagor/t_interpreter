#ifndef _PROGRAM_HPP_INCLUDED
#define _PROGRAM_HPP_INCLUDED

#include "ASTBase.hpp"
#include <vector>
#include <memory>

namespace AST 
{
  /**
   * Program node
   */
  class Program : public ASTBase
  {
    std::vector<std::unique_ptr<ASTBase>> m_functions; //!all functions in program
    public:
    Program(std::vector<std::unique_ptr<ASTBase>> &functions);
    virtual void compile(InterLang::InstructionContainer &container) override;
  };
}

#endif