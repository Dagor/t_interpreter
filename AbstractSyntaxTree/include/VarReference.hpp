#ifndef _VARREFERENCE_HPP_INCLUDED
#define _VARREFERENCE_HPP_INCLUDED

#include "ASTBase.hpp"

#include <string>

namespace AST 
{
  /**
   * Variable reference node.
   */
  class VarReference : public ASTBase
  {
    std::string m_name; //!name of the variable which to reference
    public:
    VarReference(const std::string &name);
    virtual void compile(InterLang::InstructionContainer &container) override;
  };
}

#endif