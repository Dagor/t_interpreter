#ifndef _ASTBASE_HPP_INCLUDED
#define _ASTBASE_HPP_INCLUDED

#include <string>
#include <vector>
#include <map>
#include "InstructionContainer.hpp"

namespace AST 
{
  /**
   *
   * Base abstract syntax tree class.
   * 
   */
  class ASTBase 
  {
    public:
    virtual void compile(InterLang::InstructionContainer &container) = 0;
  };
}

#endif 