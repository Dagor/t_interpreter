#ifndef _WHILELOOP_HPP_INCLUDED
#define _WHILELOOP_HPP_INCLUDED

#include "ASTBase.hpp"
#include <memory>
#include <vector>
#include <string>

namespace AST
{
  /**
   * While loop node
   * 
   */
  class WhileLoop : public ASTBase
  {
    std::string m_name; //!name of the loop
    std::unique_ptr<ASTBase> m_condition; //!loop condition
    std::vector<std::unique_ptr<ASTBase>> m_statement_list; //!statements
    public:
    WhileLoop(const std::string &name, std::unique_ptr<ASTBase> &cond,
      std::vector<std::unique_ptr<ASTBase>> &statements);
    virtual void compile(InterLang::InstructionContainer &container) override;
  };
}

#endif