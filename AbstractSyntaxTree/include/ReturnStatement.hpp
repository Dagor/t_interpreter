#ifndef _RETURNSTATEMENT_HPP_INCLUDED
#define _RETURNSTATEMENT_HPP_INCLUDED

#include "ASTBase.hpp"
#include <memory>

namespace AST
{
  /**
   * Return statement node.
   */
  class ReturnStatement : public ASTBase
  {
    std::unique_ptr<ASTBase> m_expression; //!expression node
    public:
    ReturnStatement();
    ReturnStatement(std::unique_ptr<ASTBase> &expr);
    virtual void compile(InterLang::InstructionContainer &container) override;
  };
}

#endif