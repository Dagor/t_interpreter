#ifndef _BINARYOPERATOR_HPP_INCLUDED
#define _BINARYOPERATOR_HPP_INCLUDED

#include "ASTBase.hpp"
#include "OperatorTypes.hpp"
#include <memory>

namespace AST
{
  /**
   * Binary operator node.
   */
  class BinaryOperator : public ASTBase 
  {
    std::unique_ptr<ASTBase> m_left; //!left operand
    std::unique_ptr<ASTBase> m_right; //!right operand
    OperatorType m_operator; //!type of the operator
    public:
    BinaryOperator(std::unique_ptr<ASTBase> &left, OperatorType oper, 
                std::unique_ptr<ASTBase> &right);
    
    virtual void compile(InterLang::InstructionContainer &cont) override;
  };
}

#endif