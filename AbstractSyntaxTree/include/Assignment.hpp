#ifndef _ASSIGNMENT_HPP_INCLUDED
#define _ASSIGNMENT_HPP_INCLUDED

#include "ASTBase.hpp"
#include <string>
#include <memory>

namespace AST
{
  /**
   * Assignment node of abstract syntax tree.
   * 
   */
  class Assignment : public ASTBase
  {
    std::string m_varName; //!name of variable for assignment
    std::unique_ptr<ASTBase> m_expression; //!pointer to expression node
    public:
    Assignment(const std::string &name);
    Assignment(const std::string &name, std::unique_ptr<ASTBase> &expr);
    virtual void compile(InterLang::InstructionContainer &container) override;
  };
}

#endif