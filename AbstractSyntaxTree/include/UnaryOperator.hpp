#ifndef _UNARYOPERATOR_HPP_INCLUDED
#define _UNARYOPERATOR_HPP_INCLUDED

#include "ASTBase.hpp"
#include "OperatorTypes.hpp"
#include <memory>

namespace AST
{
  /**
   * Unary operator node.
   */
  class UnaryOperator : public ASTBase
  {
    std::unique_ptr<ASTBase> m_operand; //!operand
    OperatorType m_oper; //!operator type
    public:
    UnaryOperator(OperatorType oper, std::unique_ptr<ASTBase> &operand);
    virtual void compile(InterLang::InstructionContainer &container) override;
  };
}

#endif