#ifndef _STATEMENT_HPP_INCLUDED
#define _STATEMENT_HPP_INCLUDED

#include "ASTBase.hpp"
#include <memory>

namespace AST 
{
  /**
   * Statement node.
   */
  class Statement : public ASTBase
  {
    std::unique_ptr<AST::ASTBase> m_ptr; //!ptr to more descriptive statement
    public:
    Statement(std::unique_ptr<AST::ASTBase> &node);
    virtual void compile(InterLang::InstructionContainer &container) override;
  };
}

#endif