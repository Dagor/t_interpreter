#ifndef _FUNCTION_HPP_INCLUDED
#define _FUNCTION_HPP_INCLUDED

#include "ASTBase.hpp"
#include <string>
#include <vector>
#include <memory>

namespace AST
{
  /**
   * Function node of the AST.
   * 
   */
  class Function : public ASTBase
  {
    std::string m_functionName; //!name of the function
    std::vector<std::string> m_args; //!function arguments
    std::vector<std::unique_ptr<AST::ASTBase>> m_statements; //!statement nodes
    public:
    Function(const std::string &functionName, 
      const std::vector<std::string> &args,
      std::vector<std::unique_ptr<AST::ASTBase>> &vec);
    virtual void compile(InterLang::InstructionContainer &container) override;
  };
}

#endif