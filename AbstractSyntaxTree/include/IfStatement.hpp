#ifndef _IFSTATEMENT_HPP_INCLUDED
#define _IFSTATEMENT_HPP_INCLUDED

#include "ASTBase.hpp"
#include <memory>
#include <vector>
#include <string>

namespace AST
{
  /**
   * If statement node.
   * 
   */
  class IfStatement : public ASTBase
  {
    std::vector<std::unique_ptr<ASTBase>> m_statement_list; //!statements
    std::unique_ptr<ASTBase> m_condition; //! condition to check
    std::string m_name; //!name of the loop

    public:
    IfStatement(const std::string &name, std::unique_ptr<ASTBase> &cond,
      std::vector<std::unique_ptr<ASTBase>> &statement_list);
    
    virtual void compile(InterLang::InstructionContainer &container);
  };
}

#endif