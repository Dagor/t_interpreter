#ifndef _OPERATORTYPES_HPP_INCLUDED
#define _OPERATORTYPES_HPP_INCLUDED

namespace AST
{
  /**
   * Enum containing types of operators.
   */
  typedef enum
  {
    PLUS,
    MINUS,
    MUL,
    DIV,
    EQ,
    NEQ,
    LESS,
    LESS_EQ,
    MORE,
    MORE_EQ
  }OperatorType;
}

#endif