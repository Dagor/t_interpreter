#ifndef _FUNCTIONCALL_HPP_INCLUDED
#define _FUNCTIONCALL_HPP_INCLUDED

#include "ASTBase.hpp"
#include <string>
#include <vector>
#include <memory>

namespace AST
{
  /**
   * Function call node.
   */
  class FunctionCall : public ASTBase
  {
    std::string m_name; //!name of the function to be called
    std::vector<std::unique_ptr<ASTBase>> m_args; //!arguments to pass
    public:
    FunctionCall(const std::string &name);
    FunctionCall(const std::string &name, 
      std::vector<std::unique_ptr<ASTBase>> &args);
    virtual void compile(InterLang::InstructionContainer &container) override;
  };
}

#endif