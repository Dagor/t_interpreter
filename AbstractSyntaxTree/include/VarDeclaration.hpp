#ifndef _VARDECLARATION_HPP_INCLUDED
#define _VARDECLARATION_HPP_INCLUDED

#include "ASTBase.hpp"

#include <string>
#include <memory>


namespace AST
{
  /**
   * Enum containing variable declaration types.
   */
  typedef enum {
    LOCAL_DECLARATION,
    GLOBAL_DECLARATION
  }DeclarationType;

  class VarDeclaration : public ASTBase
  {
    std::string m_varName; //!name of the variable
    DeclarationType m_type; //!type of declaration(global or local)
    std::unique_ptr<ASTBase> m_expression; //!optional expression for assignment

    public:
    VarDeclaration(const std::string &name, DeclarationType type);
    VarDeclaration(const std::string &name, DeclarationType type,
        std::unique_ptr<ASTBase> &expr);

    virtual void compile(InterLang::InstructionContainer &container) override;
  };
}

#endif