#include "FunctionCall.hpp"

namespace AST
{
  /**
   * FunctionCall constructor.
   * 
   * @param name of the called function
   */
  FunctionCall::FunctionCall(const std::string &name) :
    m_name(name), m_args()
    {}

  /**
   * FunctionCall constructor.
   * 
   * @param name of the called function
   * @param args arguments to pass to called function
   */
  FunctionCall::FunctionCall(const std::string &name, 
    std::vector<std::unique_ptr<ASTBase>> &args) :
    m_name(name), m_args(std::move(args))
    {}

  /**
   * Compile function call into intermediate language.
   * 
   * @param container containing instructions and labels
   */
  void FunctionCall::compile(InterLang::InstructionContainer &container)
  {

    for(auto &i: m_args)
    {
      i->compile(container);
      InterLang::Instruction pass_arg(InterLang::PASS_ARG);
      container.instructions.push_back(pass_arg);
    }

    if(m_name == "println")
    {
      InterLang::Instruction println(InterLang::PRINTLN);
      container.instructions.push_back(println);
    }
    else if(m_name == "console_get_int")
    {
      InterLang::Instruction get_int(InterLang::CONSOLE_GET_INT);
      container.instructions.push_back(get_int);
    }
    else
    {
      InterLang::Argument arg(InterLang::LABEL, m_name);
      InterLang::Instruction call(InterLang::CALL, arg);
      container.instructions.push_back(call);
    }
  }
}