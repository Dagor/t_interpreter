#include "Program.hpp"

namespace AST 
{
  /**
   * Program contructor.
   * 
   * @param functions vector of function nodes
   */
  Program::Program(std::vector<std::unique_ptr<ASTBase>> &functions) :
    m_functions(std::move(functions))
  {}

  /**
   * Compile program into intermediate language.
   * 
   * @param container containing instructions and labels
   */
  void Program::compile(InterLang::InstructionContainer &container)
  {
    for(auto &i: m_functions)
    {
      i->compile(container);
    }
  }
}