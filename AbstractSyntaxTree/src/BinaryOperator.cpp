#include "BinaryOperator.hpp"

#include "Instruction.hpp"
#include "Argument.hpp"


namespace AST
{
  /**
   * BinaryOperator constructor.
   * 
   * @param left operand of the operator
   * @param oper operator type
   * @param right operand of the operator
   */
  BinaryOperator::BinaryOperator(std::unique_ptr<ASTBase> &left, 
                    OperatorType oper, std::unique_ptr<ASTBase> &right)
                    : m_left(std::move(left)), m_right(std::move(right)),
                    m_operator(oper)
  {
  }

  /**
   *
   * Compiles binary operator into intermediate language instructions.
   * @param container containing instructions and labels
   * 
   */
  void BinaryOperator::compile(InterLang::InstructionContainer &container)
  {
    //compile left and right node of the tree
    m_left->compile(container);
    m_right->compile(container);

    //create instruction for the program
    if(m_operator == PLUS) 
      container.instructions.push_back(InterLang::Instruction(InterLang::ADD));
    else if(m_operator == MINUS)
      container.instructions.push_back(InterLang::Instruction(InterLang::SUB));
    else if(m_operator == MUL)
      container.instructions.push_back(InterLang::Instruction(InterLang::MUL));
    else if(m_operator == DIV)
      container.instructions.push_back(InterLang::Instruction(InterLang::DIV));
    else if(m_operator == EQ)
      container.instructions.push_back(InterLang::Instruction(InterLang::COMPARE_EQ));
    else if(m_operator == NEQ)
      container.instructions.push_back(InterLang::Instruction(InterLang::COMPARE_NEQ));
    else if(m_operator == LESS)
      container.instructions.push_back(InterLang::Instruction(InterLang::COMPARE_LESS));
    else if(m_operator == LESS_EQ)
      container.instructions.push_back(InterLang::Instruction(InterLang::COMPARE_LESS_EQ));
    else if(m_operator == MORE)
      container.instructions.push_back(InterLang::Instruction(InterLang::COMPARE_MORE));
    else if(m_operator == MORE_EQ)
      container.instructions.push_back(InterLang::Instruction(InterLang::COMPARE_MORE_EQ));

  }
}