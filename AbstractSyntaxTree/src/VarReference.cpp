#include "VarReference.hpp"

namespace AST
{
  /**
   * VarReference constructor.
   * 
   * @param name of the variable to be referenced
   */
  VarReference::VarReference(const std::string &name) :
    m_name(name)
    {}

  /**
   * Compiles the node into  language.
   * 
   * @param container containing instructions and labels
   */
  void VarReference::compile(InterLang::InstructionContainer &container)
  {
    InterLang::Argument arg(InterLang::VAR_NAME, m_name);
    InterLang::Instruction instr(InterLang::LOAD_VAR, arg);
    container.instructions.push_back(instr);
  }
}