#include "Function.hpp"

namespace AST 
{
  /**
   * Function constructor.
   * 
   * @param functionName name of the function
   * @param args function arguments
   * @param vec vector containing statements
   */
  Function::Function(const std::string &functionName,
    const std::vector<std::string> &args,
    std::vector<std::unique_ptr<AST::ASTBase>> &vec) :
    m_functionName(functionName), m_args(args), m_statements(std::move(vec))
  {
  }

  /**
   * Compile function to intermediate language.
   * 
   * @param container containing instructions and labels
   */
  void Function::compile(InterLang::InstructionContainer &container)
  {
    if(container.labels.count(m_functionName))
      throw std::runtime_error("Function already exists");

    //add label to instructions
    container.labels[m_functionName] = container.instructions.size();

    //load arguments as variables
    for(auto &i: m_args)
    {
      InterLang::Argument arg(InterLang::VAR_NAME, i);
      InterLang::Instruction load_passed(InterLang::LOAD_PASSED);
      InterLang::Instruction var_create(InterLang::CREATE_VAR, arg);
      InterLang::Instruction var_load(InterLang::STORE_VAR, arg);
      container.instructions.push_back(load_passed);
      container.instructions.push_back(var_create);
      container.instructions.push_back(var_load);
    }

    //compile all statements
    for(auto &i: m_statements)
    {
      i->compile(container);
    }

    //return from function
    InterLang::Instruction ret(InterLang::RETURN);
    container.instructions.push_back(ret);

  }
}