#include "ReturnStatement.hpp"

namespace AST
{
  /**
   * Default constructor.
   * Return from function without any value.
   */
  ReturnStatement::ReturnStatement() : 
    m_expression(nullptr)
  {}

  /**
   * ReturnStatement constructor.
   * 
   * @param expr expression to return
   */
  ReturnStatement::ReturnStatement(std::unique_ptr<ASTBase> &expr) :
    m_expression(std::move(expr))
  {}

  /**
   * Compile the node into intermediate language.
   * 
   * @param container containing instructions and labels
   */
  void ReturnStatement::compile(InterLang::InstructionContainer &container)
  {
    if(m_expression)
    {
      m_expression->compile(container);
      InterLang::Instruction instr(InterLang::RETURN_WITH_VAL);
      container.instructions.push_back(instr);
    }
    else
    {
      InterLang::Instruction instr(InterLang::RETURN);
      container.instructions.push_back(instr);
    }
  }    
}