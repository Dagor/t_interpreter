#include "VarDeclaration.hpp"

namespace AST 
{
  /**
   * VarDeclaration constructor.
   * Creates Variable declaration node without assignment.
   * 
   * @param name of the variable
   * @param type of the declaration
   */
  VarDeclaration::VarDeclaration(const std::string &name, DeclarationType type) :
    m_varName(name), m_type(type), m_expression(nullptr)
  {
  }

  /**
   * VarDeclaration constructor.
   * Creates Variable declaration node with assignment.
   * 
   * @param name of the variable
   * @param type of the declaration
   * @param expr expression for assignment
   */
  VarDeclaration::VarDeclaration(const std::string &name, DeclarationType type, 
    std::unique_ptr<ASTBase> &expr) :
    m_varName(name), m_type(type), m_expression(std::move(expr))
  {
  }

  void VarDeclaration::compile(InterLang::InstructionContainer &container)
  {
    //compile the expression attached to variable declaration
    if(m_expression)
      m_expression->compile(container);

    //create variable instruction
    InterLang::Argument arg(InterLang::VAR_NAME, m_varName);
    InterLang::Instruction instr(InterLang::CREATE_VAR, arg);
    container.instructions.push_back(instr);

    //if the expression gets executed add a store instruction
    if(m_expression)
    {
        InterLang::Instruction instr2(InterLang::STORE_VAR, arg);
        container.instructions.push_back(instr2);
    }    
  }
}