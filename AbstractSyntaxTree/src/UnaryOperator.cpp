#include "UnaryOperator.hpp"

namespace AST
{
  /**
   * UnaryOperator constructor.
   * 
   * @param oper operator type
   * @param operand node
   */
  UnaryOperator::UnaryOperator(OperatorType oper, 
      std::unique_ptr<ASTBase> &operand) :
      m_operand(std::move(operand)), m_oper(oper)
  {}

  void UnaryOperator::compile(InterLang::InstructionContainer &container)
  {
    //compile the operand code
    m_operand->compile(container);

    //minus negates the topmost value on stack
    if(m_oper == MINUS)
      container.instructions.push_back(InterLang::NEGATE);
    //plus operator does nothing
    else if(m_oper == PLUS)
      ;
    else
      throw std::runtime_error("Unknown operation.");
  }
}