#include "WhileLoop.hpp"

namespace AST
{
  /**
   * WhileLoop constructor.
   * 
   * @param name of the loop
   * @param cond condition to be checked
   * @param statements to be executed in the loop
   */
  WhileLoop::WhileLoop(const std::string &name, std::unique_ptr<ASTBase> &cond,
    std::vector<std::unique_ptr<ASTBase>> &statements) :
    m_name(name), m_condition(std::move(cond)), 
    m_statement_list(std::move(statements))
    {}

  /**
   * Compiles the node into intermediate language.
   * 
   * @param container containing instructions and labels.
   */
  void WhileLoop::compile(InterLang::InstructionContainer &container)
  {
    //create labels
    std::string condition_label(m_name + "_cond");
    std::string end_label(m_name + "_end");
    
    container.labels[condition_label] = container.instructions.size();

    m_condition->compile(container);

    //add condition label
    InterLang::Argument arg(InterLang::LABEL, end_label);
    InterLang::Instruction instr(InterLang::JUMP_IF_FALSE, arg);
    container.instructions.push_back(instr);

    //compile all statements
    for(auto &i: m_statement_list)
    {
      i->compile(container);
    }

    //jump to condition
    InterLang::Argument condition_arg(InterLang::LABEL, condition_label);
    InterLang::Instruction jump_to_cond_instr(InterLang::JUMP, condition_arg);
    container.instructions.push_back(jump_to_cond_instr);

    //add end label
    container.labels[end_label] = container.instructions.size();
  }
}