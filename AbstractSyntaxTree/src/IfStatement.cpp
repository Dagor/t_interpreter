#include "IfStatement.hpp"

namespace AST
{
  /**
   * IfStatement constructor
   * 
   * @param name of the if statement
   * @param cond node with condition
   * @param statements statements inside if block
   */
  IfStatement::IfStatement(const std::string &name, 
    std::unique_ptr<ASTBase> &cond, 
    std::vector<std::unique_ptr<ASTBase>> &statements) :
    m_statement_list(std::move(statements)), m_condition(std::move(cond)),
    m_name(name)
  {}

  /**
   * Compiles the node into intermediate language.
   * 
   * @param container containing instructions and labels
   */
  void IfStatement::compile(InterLang::InstructionContainer &container)
  {
    //std::string condition_label(m_name + "_cond");
    std::string end_label(m_name + "_end");

    //container.labels[condition_label] = container.instructions.size(); 

    m_condition->compile(container);

    InterLang::Argument arg(InterLang::LABEL, end_label);
    InterLang::Instruction instr(InterLang::JUMP_IF_FALSE, arg);
    container.instructions.push_back(instr);

    for(auto &i: m_statement_list)
    {
      i->compile(container);
    }

    container.labels[end_label] = container.instructions.size();
  }
}