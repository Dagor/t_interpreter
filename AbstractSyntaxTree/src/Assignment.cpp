#include "Assignment.hpp"

namespace AST 
{
  /**
   * Assignment constructor.
   * Creates assignment node without expression.
   * 
   * @param name variable name
   */
  Assignment::Assignment(const std::string &name) :
    m_varName(name), m_expression(nullptr)
  {}

  /**
   * Assignment constructor.
   * Creates assignment node with expression
   * 
   * @param name variable name
   * @param expr reference to pointer to expression node
   */
  Assignment::Assignment(const std::string &name, 
    std::unique_ptr<AST::ASTBase> &expr) : 
    m_varName(name), m_expression(std::move(expr))
  {}

  /**
   * Compile node into intermediate language.
   * 
   * @param container containing instructions and labels
   */
  void Assignment::compile(InterLang::InstructionContainer &container)
  {
    //skip the code if the statement has no effect
    if(m_expression){
      m_expression->compile(container);

      InterLang::Argument arg(InterLang::VAR_NAME, m_varName);
      InterLang::Instruction instr(InterLang::STORE_VAR, arg);
      container.instructions.push_back(instr);
    }

  }
}