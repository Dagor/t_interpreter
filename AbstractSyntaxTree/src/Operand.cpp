#include "Operand.hpp"

#include "Instruction.hpp"
#include "Argument.hpp"

namespace AST 
{
  /**
   * Operand constructor.
   * Creates integer literal.
   * 
   * @param val integer value
   */
  Operand::Operand(int val) :
    m_operand(val)
    {
    }

  /**
   * Operand constructor.
   * Creates string literal.
   * 
   * @param str string value
   */
  Operand::Operand(const std::string &str) :
    m_operand(str)
    {
    }

  /**
   *
   * Compiles operand into immediate language.
   * @param container containing instructions and labels
   * 
   */
  void Operand::compile(InterLang::InstructionContainer &container)
  {
    //check if operand is an integer
    if(std::holds_alternative<int>(m_operand))
    {
      //return an instruction wchich loads integer
      InterLang::Argument arg(InterLang::INT_CONST, std::get<int>(m_operand));
      InterLang::Instruction instr(InterLang::LOAD_CONST, arg);
      container.instructions.push_back(instr);
    }
    else if(std::holds_alternative<std::string>(m_operand))
    {
      //create an instruction wich loads a string
      InterLang::Argument arg(InterLang::STRING_CONST, 
                    std::get<std::string>(m_operand));

      InterLang::Instruction instr(InterLang::LOAD_CONST, arg);
      container.instructions.push_back(instr);
    }
  }
}
