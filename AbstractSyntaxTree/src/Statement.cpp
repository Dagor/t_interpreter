#include "Statement.hpp"

namespace AST 
{
  /**
   * Statement constructor.
   * 
   * @param node the statement node
   */
  Statement::Statement(std::unique_ptr<ASTBase> &node) : 
    m_ptr(std::move(node))
  {}

  /**
   * Compile statement into intermediate language.
   * 
   * @param container containing instructions and labels
   */
  void Statement::compile(InterLang::InstructionContainer &container)
  {
    m_ptr->compile(container);

    InterLang::Instruction instr(InterLang::CLEAR_STACK);
    container.instructions.push_back(instr);
  }
}