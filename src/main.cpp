#include "Instruction.hpp"
#include "Interpreter.hpp"
#include <iostream>
#include <fstream>
#include <string>
#include "ASTBase.hpp"
#include "Parser.hpp"
#include "InstructionContainer.hpp"

int main(int argc, char **argv)
{
  std::string str;
  if(argc >= 2)
  {
    std::ifstream infile(argv[1]);
    std::string line;
    while(std::getline(infile, line))
    {
      str += line;
      str += '\n';
    }
  }
  else
  {
    std::cerr << "No input file. Exiting." << std::endl;
    return -1;
  }
  Parser::Parser parser(str);
  std::unique_ptr<AST::ASTBase> ptr = parser.parse();
  InterLang::InstructionContainer container;
  ptr->compile(container);
  Interpreter::Interpreter interpreter(container);
  interpreter.Interpret();

  return 0;
}