#ifndef _ARGUMENT_HPP_INCLUDED
#define _ARGUMENT_HPP_INCLUDED

#include <variant>
#include <string>

namespace InterLang
{
  /**
   * Enum with possible instruction argument types.
   */
  typedef enum
  {
    NONE,
    INT_CONST,
    STRING_CONST,
    VAR_NAME,
    LABEL
  }ArgType;

  /**
   * Argument class.
   * 
   * The class is an argument to an instruction.
   */
  class Argument
  {
    ArgType m_type;  //!type of argument
    std::variant<int, std::string> m_val; //!value of argument
    public:
    Argument();
    Argument(ArgType arg, int val);
    Argument(ArgType arg, std::string &str);
    int getInt() const;
    std::string getString() const;
    /**
     * Argument type getter function.
     * 
     * @return ArgType type of argument
     */
    ArgType getType() const { return m_type; }
  };
}

#endif