#ifndef _INSTRUCTIONCONTAINER_HPP_INCLUDED
#define _INSTRUCTIONCONTAINER_HPP_INCLUDED

#include <map>
#include <string>
#include <vector>
#include "Instruction.hpp"

namespace InterLang
{
  /**
   *
   * Struct containing instructions and labels for interpreter.
   * 
   */
  struct InstructionContainer
  {
    std::map<std::string, unsigned int> labels; //!labels
    std::vector<Instruction> instructions; //!virtual machine instructions
  };
}


#endif
