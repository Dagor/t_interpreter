#ifndef _INSTRUCTION_HPP_INCLUDED
#define _INSTRUCTION_HPP_INCLUDED

#include "Argument.hpp"
#include <vector>

namespace InterLang
{
  /**
   * Enum containing possible instructions.
   */
  typedef enum
  {
    LOAD_CONST,
    ADD,
    SUB,  //subtract
    MUL,  //multiply
    DIV,  //divide
    POP,  //pop element off the stack
    CREATE_VAR,
    STORE_VAR,
    LOAD_VAR,
    CLEAR_STACK,
    NEGATE,
    COMPARE_EQ,
    COMPARE_NEQ,
    COMPARE_LESS,
    COMPARE_LESS_EQ,
    COMPARE_MORE,
    COMPARE_MORE_EQ,
    JUMP_IF_FALSE,
    JUMP,
    LOAD_PASSED,
    PASS_ARG,
    CALL,
    RETURN,
    RETURN_WITH_VAL,
    PRINTLN,
    CONSOLE_GET_INT
  }Operation;

  /**
   * Instruction class.
   * 
   * Class acts as an instruction to the interpreter.
   */
  class Instruction
  {
    Operation m_operation; //!instruction type
    Argument m_arg;  //!optional argument
    public:
    Instruction(Operation oper);
    Instruction(Operation oper, const Argument &arg);
    /**
     * Operation getter function.
     * 
     * @return Operation to be performed.
     */
    Operation getOperation() {return m_operation; }
    /**
     * Argument getter function.
     * 
     * @return Argument contained by getter function.
     */
    Argument getArg() {return m_arg; }
  };
}

#endif