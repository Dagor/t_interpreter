#include "Argument.hpp"

namespace InterLang
{
  /**
   * Default constructor.
   * 
   * Creates empty argument
   */
  Argument::Argument() :
    m_type(NONE), m_val(0)
  {
  }

  /**
   * Argument constructor.
   * 
   * @param arg type of the argument
   * @param val value of the argument
   */
  Argument::Argument(ArgType arg, int val) :
    m_type(arg), m_val(val)
  {
  }

  /**
   * Argument constructor.
   * 
   * @param arg type of the argument
   * @param string value of the argument
   */
  Argument::Argument(ArgType arg, std::string &str) :
    m_type(arg), m_val(str)
  {
  }

  /**
   * Get integer value from class.
   * 
   * @return int value.
   */
  int Argument::getInt() const
  {
      return std::get<int>(m_val);
  }

  /**
   * Get string value from class.
   * 
   * @return string containing the value
   */
  std::string Argument::getString() const
  {
      return std::get<std::string>(m_val);
  }
}