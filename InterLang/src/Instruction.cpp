#include "Instruction.hpp"

namespace InterLang
{
  /**
   * Instruction constructor.
   * 
   * @param oper operation to perform when interpreting
   */
  Instruction::Instruction(Operation oper) :
    m_operation(oper), m_arg()
  {
  }

  /**
   * Instruction constructor.
   * 
   * @param oper operation to perform when interpreting
   * @param arg instruction argument
   */  
  Instruction::Instruction(Operation oper, const Argument &arg) :
    m_operation(oper), m_arg(arg)
  {
  }
}