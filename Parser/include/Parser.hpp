#ifndef _PARSER_HPP_INCLUDED
#define _PARSER_HPP_INCLUDED

#include <memory>
#include <vector>
#include <string>
#include "Lexer.hpp"
#include "ASTBase.hpp"

namespace Parser 
{
  /**
   * 
   * Parser class.
   * 
   * Class can parse the tokens into abstract syntax tree.
   */
  class Parser
  {
    Lexer::Lexer m_lexer; //!lexer with text
    Lexer::Token m_current_token; //!the token being parsed
    std::string m_current_function; //!name of function currently parsed
    unsigned int m_if_counter; //!counter for if statements in function
    unsigned int m_loop_counter; //!counter for loops in function

    void discard_token(Lexer::TokenType type);

    //expression part
    std::unique_ptr<AST::ASTBase> function_call(const std::string &name);
    std::unique_ptr<AST::ASTBase> factor();
    std::unique_ptr<AST::ASTBase> term();
    std::unique_ptr<AST::ASTBase> expr();
    std::unique_ptr<AST::ASTBase> comparison_expr();

    //variables
    std::unique_ptr<AST::ASTBase> variable_declaration();
    std::unique_ptr<AST::ASTBase> var_assignment();

    //statement
    std::unique_ptr<AST::ASTBase> statement();
    std::unique_ptr<AST::ASTBase> return_statement();
    std::unique_ptr<AST::ASTBase> if_statement();
    std::unique_ptr<AST::ASTBase> while_loop();

    //function parser
    std::unique_ptr<AST::ASTBase> function();
    std::vector<std::string> arg_declarations();

    //program
    std::unique_ptr<AST::ASTBase> program();
    public:
    Parser(const Lexer::Lexer &lexer);
    Parser(const std::string &str);
    std::unique_ptr<AST::ASTBase> parse();
  };
}

#endif