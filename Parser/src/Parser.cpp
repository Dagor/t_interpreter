#include "Parser.hpp"

#include "Operand.hpp"
#include "BinaryOperator.hpp"
#include "UnaryOperator.hpp"
#include "VarDeclaration.hpp"
#include "Assignment.hpp"
#include "Function.hpp"
#include "Statement.hpp"
#include "IfStatement.hpp"
#include "WhileLoop.hpp"
#include "VarReference.hpp"
#include "FunctionCall.hpp"
#include "Program.hpp"
#include "ReturnStatement.hpp"

namespace Parser
{
  /**
   * 
   * Parser constructor.
   * 
   * @param lexer Lexer with the program to parse.
   */
  Parser::Parser(const Lexer::Lexer &lexer) :
    m_lexer(lexer), m_current_token(m_lexer.get_next_token())
    {
    }

  /**
   * Parser constructor.
   * 
   * @param str string which to parse.
   */  
  Parser::Parser(const std::string &str) :
    m_lexer(str), m_current_token(m_lexer.get_next_token())
    {
    }

  /**
   * Function parses the program.
   * 
   * @return unique_ptr to abstract syntax tree
   */
  std::unique_ptr<AST::ASTBase> Parser::parse()
  {
    return program();
  }

  /**
   *
   * Discards current token, checks if it is the proper type
   * and fetches new token from the lexer.
   * 
   * @param type of the expected token
   */
  void Parser::discard_token(Lexer::TokenType type)
  {
    if(type == m_current_token.getType())
      m_current_token = m_lexer.get_next_token();
    else
      throw std::runtime_error("Wrong token.");
  }

  /**
   * 
   * Parse the function call.
   * 
   * @return unique_ptr containing function call node
   */
  std::unique_ptr<AST::ASTBase> Parser::function_call(const std::string &name)
  {
    discard_token(Lexer::LPAREN);
    std::unique_ptr<AST::ASTBase> fun;
    if(m_current_token.getType() == Lexer::RPAREN)
    {
      fun = std::make_unique<AST::FunctionCall>(name);
      discard_token(Lexer::RPAREN);
    }
    else
    {
      std::vector<std::unique_ptr<AST::ASTBase>> args;
      //get first argument
      std::unique_ptr<AST::ASTBase> node = comparison_expr();
      args.push_back(std::move(node));

      //get the rest of arguments
      while(m_current_token.getType() == Lexer::COMMA)
      {
        discard_token(Lexer::COMMA);
        node = comparison_expr();
      }
      discard_token(Lexer::RPAREN);

      fun = std::make_unique<AST::FunctionCall>(name, args);
    }

    return fun;
  }
  
  /**
   *
   * Function for parsing factor part of the syntax.
   *
   * @return unique_ptr containing factor node 
   */
  std::unique_ptr<AST::ASTBase> Parser::factor()
  {
    std::unique_ptr<AST::ASTBase> node;

    if(m_current_token.getType() == Lexer::INTEGER)
    {
      int val = m_current_token.getInt();
      discard_token(Lexer::INTEGER);
      node = std::make_unique<AST::Operand>(val);
    }
    else if(m_current_token.getType() == Lexer::STRING)
    {
      std::string val = m_current_token.getString();
      discard_token(Lexer::STRING);
      node = std::make_unique<AST::Operand>(val);
    }
    else if(m_current_token.getType() == Lexer::MINUS)
    {
      discard_token(Lexer::MINUS);
      std::unique_ptr<AST::ASTBase> operand = comparison_expr();
      node = std::make_unique<AST::UnaryOperator>(AST::MINUS, operand);
    }
    else if(m_current_token.getType() == Lexer::PLUS)
    {
      discard_token(Lexer::PLUS);
      std::unique_ptr<AST::ASTBase> operand = comparison_expr();
      node = std::make_unique<AST::UnaryOperator>(AST::PLUS, operand);
    }
    else if(m_current_token.getType() == Lexer::LPAREN)
    {
      discard_token(Lexer::LPAREN);
      node = comparison_expr();
      discard_token(Lexer::RPAREN);
    }
    else if(m_current_token.getType() == Lexer::IDENTIFIER)
    {
      std::string name(m_current_token.getString());
      discard_token(Lexer::IDENTIFIER);
      node = std::make_unique<AST::VarReference>(name);
    }
    else if(m_current_token.getType() == Lexer::FUNCTION)
    {
      std::string name(m_current_token.getString());
      discard_token(Lexer::FUNCTION);
      node = function_call(name);
    }
    else
      throw std::runtime_error("Expected string or integer");

    return node;
  }

  /**
   *
   * Parses the term part of the expression.
   *
   * @return unique_ptr containing term node 
   */
  std::unique_ptr<AST::ASTBase> Parser::term()
  {
    //get left operand
    std::unique_ptr<AST::ASTBase> node = factor();

    //multiply and divide operations
    while(m_current_token.getType() == Lexer::MULTIPLY
      || m_current_token.getType() == Lexer::DIVIDE)
    {
      if(m_current_token.getType() == Lexer::MULTIPLY)
      {
        discard_token(Lexer::MULTIPLY);
        std::unique_ptr<AST::ASTBase> rhs = factor();
        node = std::make_unique<AST::BinaryOperator>(node, AST::MUL, rhs);
      }
      else if(m_current_token.getType() == Lexer::DIVIDE)
      {
        discard_token(Lexer::DIVIDE);
        std::unique_ptr<AST::ASTBase> rhs = factor();
        node = std::make_unique<AST::BinaryOperator>(node, AST::DIV, rhs);
      }
    }

    return node;
  }

  /**
   *
   * Parse the expression.
   *
   * @return unique_ptr containing the expression node 
   */
  std::unique_ptr<AST::ASTBase> Parser::expr()
  {
    //get left hand operator
    std::unique_ptr<AST::ASTBase> node = term();

    //parse the addition and subtraction
    while(m_current_token.getType() == Lexer::PLUS
      || m_current_token.getType() == Lexer::MINUS)
    {
      if(m_current_token.getType() == Lexer::PLUS)
      {
        discard_token(Lexer::PLUS);
        std::unique_ptr<AST::ASTBase> rhs = term();
        node = std::make_unique<AST::BinaryOperator>(node, AST::PLUS, rhs);
      }
      else if(m_current_token.getType() == Lexer::MINUS)
      {
        discard_token(Lexer::MINUS);
        std::unique_ptr<AST::ASTBase> rhs = term();
        node = std::make_unique<AST::BinaryOperator>(node, AST::MINUS, rhs);
      }
    }

    return node;
  }

  /**
   *
   * Parse comparison operators.
   * 
   * @return unique_ptr containing node of the whole expression
   */
  std::unique_ptr<AST::ASTBase> Parser::comparison_expr()
  {
    std::unique_ptr<AST::ASTBase> node = expr();

    Lexer::TokenType type = m_current_token.getType();
    while(type == Lexer::COMP_EQ || type == Lexer::COMP_NEQ
      || type == Lexer::COMP_LESS || type == Lexer::COMP_LESS_EQ
      || type == Lexer::COMP_MORE || type == Lexer::COMP_MORE_EQ)
    {
      if(type == Lexer::COMP_EQ)
      {
        discard_token(Lexer::COMP_EQ);
        std::unique_ptr<AST::ASTBase> rhs = expr();
        node = std::make_unique<AST::BinaryOperator>(node, AST::EQ, rhs);
      }
      else if(type == Lexer::COMP_NEQ)
      {
        discard_token(Lexer::COMP_NEQ);
        std::unique_ptr<AST::ASTBase> rhs = expr();
        node = std::make_unique<AST::BinaryOperator>(node, AST::NEQ, rhs);
      }
      else if(type == Lexer::COMP_LESS)
      {
        discard_token(Lexer::COMP_LESS);
        std::unique_ptr<AST::ASTBase> rhs = expr();
        node = std::make_unique<AST::BinaryOperator>(node, AST::LESS, rhs);
      }
      else if(type == Lexer::COMP_LESS_EQ)
      {
        discard_token(Lexer::COMP_LESS_EQ);
        std::unique_ptr<AST::ASTBase> rhs = expr();
        node = std::make_unique<AST::BinaryOperator>(node, AST::LESS_EQ, rhs);
      }
      else if(type == Lexer::COMP_MORE)
      {
        discard_token(Lexer::COMP_MORE);
        std::unique_ptr<AST::ASTBase> rhs = expr();
        node = std::make_unique<AST::BinaryOperator>(node, AST::MORE, rhs);
      }
      else if(type == Lexer::COMP_MORE_EQ)
      {
        discard_token(Lexer::COMP_MORE_EQ);
        std::unique_ptr<AST::ASTBase> rhs = expr();
        node = std::make_unique<AST::BinaryOperator>(node, AST::MORE_EQ, rhs);
      }

      type = m_current_token.getType();
    }

    return node;
  }

  /**
   *
   * Parse the variable declarations.
   *
   * @return unique_ptr containing variable declaration node 
   */
  std::unique_ptr<AST::ASTBase> Parser::variable_declaration()
  {
    discard_token(Lexer::VAR);

    //get the variable name from the lexer
    std::string varName = m_current_token.getString();

    discard_token(Lexer::IDENTIFIER);

    if(m_current_token.getType() == Lexer::EQUAL)
    {
      //create node with assignment
      discard_token(Lexer::EQUAL);
      std::unique_ptr<AST::ASTBase> expression = comparison_expr();
      return std::make_unique<AST::VarDeclaration>(varName, 
        AST::LOCAL_DECLARATION, expression);
    }
    else
    {
      //create node without assignment
      return std::make_unique<AST::VarDeclaration>(varName,
        AST::LOCAL_DECLARATION);
    }

  }

  /**
   *
   * Parse the variable assignment.
   *
   * @return unique_ptr containing assignment node 
   */
  std::unique_ptr<AST::ASTBase> Parser::var_assignment()
  {
    //get variable name
    std::string varName(m_current_token.getString());
    discard_token(Lexer::IDENTIFIER);

    if(m_current_token.getType() == Lexer::EQUAL)
    {
      //create node with assignment
      discard_token(Lexer::EQUAL);
      std::unique_ptr<AST::ASTBase> expression = comparison_expr();
      return std::make_unique<AST::Assignment>(varName, expression);
    }

    //cresate node without assignment
    return std::make_unique<AST::Assignment>(varName);
  }

  /**
   *
   * Parse the statement.
   *
   * @return unique_ptr to statement node 
   */
  std::unique_ptr<AST::ASTBase> Parser::statement()
  {
    if(m_current_token.getType() == Lexer::IDENTIFIER)
    {
      std::unique_ptr<AST::ASTBase> node = var_assignment();
      discard_token(Lexer::SEMICOLON);
      return std::make_unique<AST::Statement>(node);
    }
    else if(m_current_token.getType() == Lexer::VAR)
    {
      std::unique_ptr<AST::ASTBase> node = variable_declaration();
      discard_token(Lexer::SEMICOLON);
      return std::make_unique<AST::Statement>(node);
    }
    else if(m_current_token.getType() == Lexer::IF)
    {
      std::unique_ptr<AST::ASTBase> node = if_statement();
      return std::make_unique<AST::Statement>(node);
    }
    else if(m_current_token.getType() == Lexer::WHILE)
    {
      std::unique_ptr<AST::ASTBase> node = while_loop();
      return std::make_unique<AST::Statement>(node);
    }
    else if(m_current_token.getType() == Lexer::RETURN)
    {
      std::unique_ptr<AST::ASTBase> node = return_statement();
      return std::make_unique<AST::Statement>(node);
    }
    else 
    {
      std::unique_ptr<AST::ASTBase> node = comparison_expr();
      discard_token(Lexer::SEMICOLON);
      return std::make_unique<AST::Statement>(node);
    }
  }

  /**
   * Parse the return statement.
   * 
   * @return unique_ptr to return statement
   */
  std::unique_ptr<AST::ASTBase> Parser::return_statement()
  {
    discard_token(Lexer::RETURN);
    if(m_current_token.getType() == Lexer::SEMICOLON)
    {
      discard_token(Lexer::SEMICOLON);
      return std::make_unique<AST::ReturnStatement>();
    }
    else
    {
      std::unique_ptr<AST::ASTBase> node = comparison_expr();
      discard_token(Lexer::SEMICOLON);
      return std::make_unique<AST::ReturnStatement>(node);
    }
  }

  /**
   * Parse if statement.
   * 
   * @return unique_ptr to if statement node
   */
  std::unique_ptr<AST::ASTBase> Parser::if_statement()
  {
    m_if_counter++;

    std::string name = m_current_function + "_if_" 
        + std::to_string(m_if_counter);

    discard_token(Lexer::IF);
    discard_token(Lexer::LPAREN);

    std::unique_ptr<AST::ASTBase> condition = comparison_expr();

    discard_token(Lexer::RPAREN);
    discard_token(Lexer::LCURLY);

    std::vector<std::unique_ptr<AST::ASTBase>> statement_list;

    while(m_current_token.getType() != Lexer::RCURLY)
    {
      std::unique_ptr<AST::ASTBase> node = statement();
      statement_list.push_back(std::move(node));
    }

    discard_token(Lexer::RCURLY);

    return std::make_unique<AST::IfStatement>(name, condition, statement_list);
  }

  /**
   * Parse while loop.
   * 
   * @return unique_ptr to while loop node
   */
  std::unique_ptr<AST::ASTBase> Parser::while_loop()
  {
    m_loop_counter++;

    std::string name = m_current_function + "_loop_" 
        + std::to_string(m_loop_counter);

    discard_token(Lexer::WHILE);
    discard_token(Lexer::LPAREN);

    std::unique_ptr<AST::ASTBase> condition = comparison_expr();

    discard_token(Lexer::RPAREN);
    discard_token(Lexer::LCURLY);

    std::vector<std::unique_ptr<AST::ASTBase>> statement_list;

    while(m_current_token.getType() != Lexer::RCURLY)
    {
      std::unique_ptr<AST::ASTBase> node = statement();
      statement_list.push_back(std::move(node));
    }

    discard_token(Lexer::RCURLY);

    return std::make_unique<AST::WhileLoop>(name, condition, statement_list);
  }

  /**
   *
   * Function parser.
   *
   * @return unique_ptr to function node 
   */
  std::unique_ptr<AST::ASTBase> Parser::function()
  {
    discard_token(Lexer::FN);

    m_current_function = m_current_token.getString();
    m_if_counter = 0;
    m_loop_counter = 0;

    discard_token(Lexer::FUNCTION);

    discard_token(Lexer::LPAREN);

    std::vector<std::string> args = arg_declarations();

    discard_token(Lexer::RPAREN);

    std::vector<std::unique_ptr<AST::ASTBase>> statementList;
    
    discard_token(Lexer::LCURLY);

    while(m_current_token.getType() != Lexer::RCURLY)
    {
      std::unique_ptr<AST::ASTBase> node = statement();
      statementList.push_back(std::move(node));
    }

    discard_token(Lexer::RCURLY);

    return std::make_unique<AST::Function>(m_current_function, args, statementList);
  }

  /**
   *
   * Function parses arguments in function declaration.
   * 
   * @return vector of strings with argument names.
   */
  std::vector<std::string> Parser::arg_declarations()
  {
    std::vector<std::string> retVal;

    //get first argument
    if(m_current_token.getType() == Lexer::VAR)
    {
      discard_token(Lexer::VAR);

      retVal.push_back(m_current_token.getString());

      discard_token(Lexer::IDENTIFIER);
    }

    //get other arguments
    while(m_current_token.getType() == Lexer::COMMA)
    {
      discard_token(Lexer::COMMA);
      discard_token(Lexer::VAR);

      retVal.push_back(m_current_token.getString());

      discard_token(Lexer::IDENTIFIER);
    }

    return retVal;
  }

  /**
   * 
   * Parse the whole program.
   * 
   * @return unique_ptr to program node.
   */
  std::unique_ptr<AST::ASTBase> Parser::program()
  {
    std::vector<std::unique_ptr<AST::ASTBase>> functions;

    while(m_current_token.getType() != Lexer::END)
    {
      std::unique_ptr<AST::ASTBase> node = function();
      functions.push_back(std::move(node));
    }

    return std::make_unique<AST::Program>(functions);
  }

}
