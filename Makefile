CXX = g++

CXXFLAGS = -std=c++17 -Wall -g3 -Og

BUILD_DIR = Build
TARGET_NAME = Interpreter_Project
INCLUDE = -IInterLang/include \
-Iinclude \
-IInterpreter/include \
-ILexer/include \
-IParser/include \
-IAbstractSyntaxTree/include \


SRC = \
$(wildcard InterLang/src/*.cpp) \
$(wildcard src/*.cpp) \
$(wildcard Interpreter/src/*.cpp) \
$(wildcard Lexer/src/*.cpp) \
$(wildcard Parser/src/*.cpp) \
$(wildcard AbstractSyntaxTree/src/*.cpp) \

OBJS = $(SRC:%.cpp=$(BUILD_DIR)/%.o)

.PHONY: all build clean

all: build $(BUILD_DIR)/$(TARGET_NAME)

$(BUILD_DIR)/$(TARGET_NAME): $(OBJS)
	mkdir -p $(@D)
	$(CXX) $^ -o $@

$(BUILD_DIR)/%.o: %.cpp
	mkdir -p $(@D)
	$(CXX) $(CXXFLAGS) $(INCLUDE) -o $@ -c $<

build:
	mkdir -p $(BUILD_DIR)

clean:
	rm -f $(OBJS)