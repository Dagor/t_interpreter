#ifndef _TOKEN_HPP_INCLUDED
#define _TOKEN_HPP_INCLUDED

#include <variant>
#include <string>

namespace Lexer
{
  /**
   * 
   * Enum containing all possible token types.
   */
  typedef enum
  {
    INTEGER,
    STRING,
    PLUS,
    MINUS,
    MULTIPLY,
    DIVIDE,
    SEMICOLON,
    COMMA,
    IDENTIFIER,
    VAR,
    FN,
    WHILE,
    IF,
    EQUAL,
    COMP_EQ,
    COMP_NEQ,
    COMP_LESS,
    COMP_LESS_EQ,
    COMP_MORE,
    COMP_MORE_EQ,
    LPAREN,  //left parenthesis
    RPAREN,  //right parenthesis
    LCURLY,  //left curly brace
    RCURLY,  //right curly brace
    FUNCTION,
    RETURN,
    END
  }TokenType;

  /**
   * 
   * Token class.
   * 
   * Contains the token type and optionally the value.
   */
  class Token 
  {
    TokenType m_type;
    std::variant<int, std::string> m_value;
    public:
    Token(TokenType type);
    Token(int val);
    Token(TokenType type, std::string val);
    /**
     * 
     * Get type of the token.
     * 
     * @return TokenType type
     */
    TokenType getType() const { return m_type ;}
    int getInt() const;
    std::string getString() const;
  };
}

#endif