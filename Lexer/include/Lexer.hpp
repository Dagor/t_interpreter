#ifndef _LEXER_HPP_INCLUDED
#define _LEXER_HPP_INCLUDED

#include "Token.hpp"
#include <string>
#include <map>

namespace Lexer 
{
  /**
   *
   * Lexer class.
   * 
   * Class can split the input string into tokens.
   */
  class Lexer 
  {
    static const std::map<std::string, Token> g_keywords; //!tokens
    std::string m_text;  //!holds the text
    unsigned int m_pos;  //!current position in text
    char m_current_char; //!current character
    unsigned int m_current_line; //!line in text

    void advance_char();
    int get_integer();
    void skip_whitespace();
    Token get_identifier();
    void skip_comment();
    char peek();
    std::string get_string();
    public:
    Lexer(const std::string &text);
    Token get_next_token();
  };
}


#endif