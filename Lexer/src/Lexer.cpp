#include "Lexer.hpp"

namespace Lexer
{
  const std::map<std::string, Token> Lexer::g_keywords{
    {"var", Token(VAR)},
    {"fn", Token(FN)},
    {"while", Token(WHILE)},
    {"if", Token(IF)},
    {"return", Token(RETURN)}
  };

  /**
   *
   * Lexer constructor
   * 
   * @param str string with the program to tokenize
   *
   */
  Lexer::Lexer(const std::string &str) :
    m_text(str), m_pos(0), m_current_char(m_text[m_pos]), m_current_line(1)
  {
  }

  /**
   * 
   * Get next token from the lexer.
   * 
   * @return Token found in the text
   */
  Token Lexer::get_next_token()
  {
    while(m_current_char)
    {
      //two character operators
      if(m_current_char == '=' && peek() == '=')
      {
        advance_char();
        advance_char();
        return Token(COMP_EQ);
      }
      if(m_current_char == '!' && peek() == '=')
      {
        advance_char();
        advance_char();
        return Token(COMP_NEQ);
      }
      if(m_current_char == '<' && peek() == '=')
      {
        advance_char();
        advance_char();
        return Token(COMP_LESS_EQ);
      }
      if(m_current_char == '>' && peek() == '=')
      {
        advance_char();
        advance_char();
        return Token(COMP_MORE_EQ);
      }
      if(m_current_char == '/' && peek() == '*')
      {
        skip_comment();
        continue;
      }

      //single character operators
      if(m_current_char == '>')
      {
        advance_char();
        return Token(COMP_MORE);
      }
      if(m_current_char == '<')
      {
        advance_char();
        return Token(COMP_LESS);
      }
      if(m_current_char == '+')
      {
        advance_char();
        return Token(PLUS);
      }
      if(m_current_char == '-')
      {
        advance_char();
        return Token(MINUS);
      }
      if(m_current_char == '*')
      {
        advance_char();
        return Token(MULTIPLY);
      }
      if(m_current_char == '/')
      {
        advance_char();
        return Token(DIVIDE);
      }
      if(m_current_char == '(')
      {
        advance_char();
        return Token(LPAREN);
      }
      if(m_current_char == ')')
      {
        advance_char();
        return Token(RPAREN);
      }
      if(m_current_char == '{')
      {
        advance_char();
        return Token(LCURLY);
      }
      if(m_current_char == '}')
      {
        advance_char();
        return Token(RCURLY);
      }
      if(m_current_char == ';')
      {
        advance_char();
        return Token(SEMICOLON);
      }
      if(m_current_char == ',')
      {
        advance_char();
        return Token(COMMA);
      }
      if(m_current_char == '=')
      {
        advance_char();
        return Token(EQUAL);
      }
      if(m_current_char == '"')
      {
        return Token(STRING, get_string());
      }
      if(std::isalpha(m_current_char))
      {
        return get_identifier();
      }
      if(std::isdigit(m_current_char))
      {
        return Token(get_integer());
      }
      if(std::isspace(m_current_char))
      {
        skip_whitespace();
        continue;
      }
    }
    //indicate end of input program
    return Token(END);
  }

  /**
   *
   * Advances the position in text.
   * 
   */
  void Lexer::advance_char()
  {
    m_pos++;

    //if position is past the length of input change the current char ro null
    if(m_pos >= m_text.size())
      m_current_char = 0;
    else
      m_current_char = m_text.at(m_pos);
  }

  /**
   *
   * Get integer from the current position in text.
   * @param none
   * @returns integer from current position in text
   * 
   */
  int Lexer::get_integer()
  {
    std::string buffer;
    while(std::isdigit(m_current_char))
    {
      //add current character to buffer and advance
      buffer += m_current_char;
      advance_char();
    }

    //convert string to integer
    return std::stoi(buffer);
  }

  /**
   *
   * Method skips whitespaces.
   * 
   */
  void Lexer::skip_whitespace()
  {
    //advance character when the whitespace is encountered
    while(std::isspace(m_current_char))
    {
      //indicate next line
      if(m_current_char == '\n')
        m_current_line++;
    
      advance_char();
    }
  }

  /**
   * 
   * Get the identifier.
   * 
   * @return Token containing an identifier keyword,
   * or function.
   * 
   */
  Token Lexer::get_identifier()
  {
    //temporary buffer for holding the identifier string
    std::string buf;

    while(std::isdigit(m_current_char) || std::isalpha(m_current_char)
      || m_current_char == '_')
    {
      //add characters to buffer
      buf += m_current_char;
      advance_char();
    }

    if(g_keywords.count(buf))
      return g_keywords.at(buf);

    if(m_current_char == '(')
      return Token(FUNCTION, buf);

    return Token(IDENTIFIER, buf);
  }

  /**
   * 
   * Peek into the next character in the string without discarding it.
   * 
   * @return character that is next
   */
  char Lexer::peek()
  {
    if(m_pos + 1 >= m_text.size())
      return '\0';
    else
      return m_text[m_pos+1];
  }

  /**
   * 
   * Skip the comment in the text.
   * 
   */
  void Lexer::skip_comment()
  {
    //skip comment
    while(!(m_current_char == '*' && peek() == '/'))
      advance_char();

    //skip comment end operator
    advance_char();
    advance_char();
  }

  /**
   * 
   * Get the string constant.
   * 
   * @return string constant
   */
  std::string Lexer::get_string()
  {
    advance_char();
    std::string buf;

    while(m_current_char != '"')
    {
      buf += m_current_char;
      advance_char();
    }

    advance_char();
    return buf;
  }

}