#include "Token.hpp"

namespace Lexer
{
  /**
   * 
   * Token constructor.
   * 
   * @param type of the token
   */
  Token::Token(TokenType type) :
    m_type(type), m_value(0)
  {
  }

  /**
   * 
   * Token constructor.
   * Sets the token type to integer.
   * 
   * @param val value of the token
   */
  Token::Token(int val) :
    m_type(INTEGER), m_value(val)
  {
  } 

  /**
   * 
   * Token constructor.
   * 
   * @param type type of token
   * @param string containig value
   */
  Token::Token(TokenType type, std::string str) :
    m_type(type), m_value(str)
  {
  }

  /**
   * 
   * Get the integer value from the string.
   * 
   * @return int value
   */
  int Token::getInt() const
  {
    return std::get<int>(m_value);
  }

  /**
   * 
   * Get string value from the token.
   * 
   * @return string value.
   */
  std::string Token::getString() const
  {
    return std::get<std::string>(m_value);
  }

}